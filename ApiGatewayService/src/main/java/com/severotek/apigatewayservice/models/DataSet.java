package com.severotek.apigatewayservice.models;

public class DataSet {

    public String name;

    public String connectionName;

    public String sqlcommand;

    public boolean isGuestAllowed;

    public boolean isViewerAllowed;

    public DataSet(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConnectionName() {
        return connectionName;
    }

    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }

    public String getSQLCommand() {
        return sqlcommand;
    }

    public void setSQLCommand(String sqlcommand) {
        this.sqlcommand = sqlcommand;
    }

    public boolean getIsGuestAllowed() {
        return isGuestAllowed;
    }

    public void setIsGuestAllowed(boolean isGuestAllowed){
        this.isGuestAllowed = isGuestAllowed;
    }

    public boolean getIsViewerAllowed(){
        return isViewerAllowed;
    }

    public void setIsViewerAllowed(boolean isViewerAllowed){
        this.isViewerAllowed = isViewerAllowed;
    }
}
