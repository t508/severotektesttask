package com.severotek.apigatewayservice.models;


public class ConnectionToDB {

    public String name;

    public String address;

    public Integer port;

    public String login;

    public String password;

    public String dbname;

    public boolean isGuestAllowed;

    public boolean isViewerAllowed;

    public ConnectionToDB(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

    public String getDbname() {
        return dbname;
    }

    public boolean getIsGuestAllowed() {
        return isGuestAllowed;
    }

    public void setIsGuestAllowed(boolean isGuestAllowed){
        this.isGuestAllowed = isGuestAllowed;
    }

    public boolean getIsViewerAllowed(){
        return isViewerAllowed;
    }

    public void setIsViewerAllowed(boolean isViewerAllowed){
        this.isViewerAllowed = isViewerAllowed;
    }
}
