package com.severotek.apigatewayservice.models;

public class DataReader {
    public String url;

    public String user;

    public String password;

    public String command;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public DataReader(String url, String user, String password, String command){
        this.url = url;
        this.user = user;
        this.password = password;
        this.command = command;
    }
}
