package com.severotek.apigatewayservice.controllers;

import com.severotek.apigatewayservice.models.ConnectionToDB;
import com.severotek.apigatewayservice.models.DataReader;
import com.severotek.apigatewayservice.models.DataSet;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/datasets")
public class DataSetController {

    @Autowired
    RabbitTemplate template;

    @Autowired
    DataSourcesController dataSourcesController;

    @PostMapping("/create")
    String createDataSet(@RequestBody DataSet set, String token) {
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        String response = "";
        if (CheckResponse.equals("ROLE_ADMIN") || CheckResponse.equals("ROLE_OPERATOR")) {
            response = (String) template.convertSendAndReceive("dataSetsController", "create", set);
        }
        else {
            response = "You cant access this method";
        }
        return response;
    }

    @GetMapping("/getAll")
    Object getAllDataSets(String token) {
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        Object response;
        if (!CheckResponse.startsWith("Error")) {
            var tmpRes = template.convertSendAndReceive("dataSetsController", "getAll", CheckResponse);
            response = tmpRes instanceof String ?  tmpRes.toString() : (List<DataSet>) tmpRes;
        }
        else {
            return CheckResponse;
        }
        return response;
    }

    @GetMapping("/getData")
    Object getDataSet(String token, String name) {
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        DataSet response;
        if (!CheckResponse.startsWith("Error")) {
            var tmpRes = template.convertSendAndReceive("dataSetsController", "get", name);
            if (tmpRes instanceof String)
                return tmpRes.toString();
            else response = (DataSet) tmpRes;
        }
        else {
            return CheckResponse;
        }
        if ((CheckResponse.equals("ROLE_GUEST") && !response.getIsGuestAllowed()) ||
                (CheckResponse.equals("ROLE_VIEWER") && !response.getIsViewerAllowed())){
            return "Error: you doesnt have rights to get info about this DataSource";
        }
        else
            return response;
    }

    @GetMapping("/readData")
    Object readData(String token, String name) {
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        DataSet response;
        ConnectionToDB con;
        if (!CheckResponse.startsWith("Error")) {
            var tmpRes = getDataSet(token, name);
            if (tmpRes instanceof String)
                return tmpRes.toString();
            else
                response = (DataSet) tmpRes;
        }
        else {
            return CheckResponse;
        }
        var tmpCon = dataSourcesController.getDataSource(token, response.getConnectionName());
        if (tmpCon instanceof String)
            return tmpCon.toString();
        else
            con = (ConnectionToDB) tmpCon;
        String url = "jdbc:" + con.getAddress() + ":" + con.getPort().toString() + con.getDbname();
        DataReader params = new DataReader(url, con.getLogin(), con.getPassword(), response.getSQLCommand());

        var tmpFinalResult = template.convertSendAndReceive("dataSetsController","readData", params);
        if (tmpFinalResult instanceof String)
            return tmpFinalResult.toString();
        else
            return (List<Object[]>) tmpFinalResult;
    }

    @GetMapping("/delete")
    String deleteDataSet(String token, String name) {
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        String response = "";
        if (CheckResponse.equals("ROLE_ADMIN") || CheckResponse.equals("ROLE_OPERATOR")) {
            response = template.convertSendAndReceive("dataSetsController", "delete", name).toString();
        }
        else {
            return CheckResponse;
        }
        return response;
    }

    @PostMapping("/update")
    String updateDataSet(String token, @RequestBody DataSet con) {
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        String response = "";
        if (CheckResponse.equals("ROLE_ADMIN") || CheckResponse.equals("ROLE_OPERATOR")) {
            response = template.convertSendAndReceive("dataSetsController", "update", con).toString();
        }
        else {
            return CheckResponse;
        }
        return response;
    }

}
