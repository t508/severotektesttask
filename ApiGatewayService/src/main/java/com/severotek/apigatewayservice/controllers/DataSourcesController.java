package com.severotek.apigatewayservice.controllers;

import com.severotek.apigatewayservice.models.ConnectionToDB;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/datasources")
public class DataSourcesController {

    @Autowired
    RabbitTemplate template;

    @PostMapping("/create")
    String createDataSource(@RequestBody ConnectionToDB con, String token) {
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        String response = "";
        if (CheckResponse.equals("ROLE_ADMIN") || CheckResponse.equals("ROLE_OPERATOR")) {
            response = (String) template.convertSendAndReceive("dataSourceController", "create", con);
        }
        else {
            response = "You cant access this method";
        }
        return response;
    }

    @GetMapping("/get")
    Object getDataSource(String token, String name) {
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        ConnectionToDB response;
        if (!CheckResponse.startsWith("Error")) {
            var tmpRes = template.convertSendAndReceive("dataSourceController", "get", name);
            if (tmpRes instanceof String)
                return tmpRes.toString();
            else response = (ConnectionToDB) tmpRes;
        }
        else {
            return CheckResponse;
        }
        if ((CheckResponse.equals("ROLE_GUEST") && !response.getIsGuestAllowed()) ||
                (CheckResponse.equals("ROLE_VIEWER") && !response.getIsViewerAllowed())){
            return "Error: you doesnt have rights to get info about this DataSource";
        }
        else
            return response;
    }

    @GetMapping("/getAll")
    Object getAllDataSource(String token) {
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        Object response;
        if (!CheckResponse.startsWith("Error")) {
            var tmpRes = template.convertSendAndReceive("dataSourceController", "getAll", CheckResponse);
            response = tmpRes instanceof String ?  tmpRes.toString() : (List<ConnectionToDB>) tmpRes;
        }
        else {
            return CheckResponse;
        }
        return response;
    }

    @GetMapping("/delete")
    String deleteDataSource(String token, String name) {
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        String response = "";
        if (CheckResponse.equals("ROLE_ADMIN") || CheckResponse.equals("ROLE_OPERATOR")) {
            response = template.convertSendAndReceive("dataSourceController", "delete", name).toString();
        }
        else {
            return CheckResponse;
        }
        return response;
    }

    @PostMapping("/update")
    String updateDataSource(String token, @RequestBody ConnectionToDB con) {
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        String response = "";
        if (CheckResponse.equals("ROLE_ADMIN") || CheckResponse.equals("ROLE_OPERATOR")) {
            response = template.convertSendAndReceive("dataSourceController", "update", con).toString();
        }
        else {
            return CheckResponse;
        }
        return response;
    }
}
