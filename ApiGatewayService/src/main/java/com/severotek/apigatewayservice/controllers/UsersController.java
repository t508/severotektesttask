package com.severotek.apigatewayservice.controllers;

import com.severotek.apigatewayservice.models.User;
import com.severotek.apigatewayservice.payloads.LoginParams;
import com.severotek.apigatewayservice.payloads.UpdateParams;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UsersController {

    @Autowired
    RabbitTemplate template;

    @PostMapping("/login")
    String loginUserToSystem(@RequestBody LoginParams params) {
        String response = template.convertSendAndReceive("userController","login", params).toString();
        return response;
    }
    @PostMapping("/create")
    String createUser(@RequestBody User user, String token) {
        String NeededRole = "ROLE_ADMIN";
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        String response = "";
        if (CheckResponse.equals(NeededRole)) {
            response = (String) template.convertSendAndReceive("userController", "create", user);
        }
        else {
            response = "You doesnt have permission for this";
        }
        return response;
    }
    @GetMapping("/delete")
    String deleteUser(String username, String token) {
        String NeededRole = "ROLE_ADMIN";
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        String response = "";
        if (CheckResponse.equals(NeededRole)){
            response = (String) template.convertSendAndReceive("userController","delete", username);
        }
        else {
            response = "You doesnt have permission for this";
        }
        return response;
    }
    @GetMapping("/updateUserRole")
    String updateUserRole(String token, String username, String role) {
        String NeededRole = "ROLE_ADMIN";
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        String response = "";
        if (CheckResponse.equals(NeededRole)){
            UpdateParams params = new UpdateParams(username,role);
            response = (String) template.convertSendAndReceive("userController","updateRole", params);
        }
        else {
            response = "You doesnt have permission for this";
        }
        return response;
    }
    @GetMapping("/updateUsername")
    String updateUsername(String token, String username, String newUsername) {
        String NeededRole = "ROLE_ADMIN";
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        String response = "";
        if (CheckResponse.equals(NeededRole)){
            UpdateParams params = new UpdateParams(username, newUsername);
            response = (String) template.convertSendAndReceive("userController","updateUsername", params);
        }
        else {
            response = "You doesnt have permission for this";
        }
        return response;
    }

    @GetMapping("/get")
    Object getUser(String token, String username) {
        String NeededRole = "ROLE_ADMIN";
        String CheckResponse = (String)template.convertSendAndReceive("userController","validate", token);
        Object response = "";
        if (CheckResponse.equals(NeededRole)){
            var tmpResp = template.convertSendAndReceive("userController","get", username);
            response = tmpResp instanceof User ? (User) tmpResp : tmpResp.toString();
        }
        else {
            response = "You doesnt have permission for this";
        }
        return response;
    }
}
