package com.severotek.apigatewayservice.payloads;

public class LoginParams {
    public String username;

    public String password;

    public LoginParams (String _username, String _password){
        this.username = _username;
        this.password = _password;
    }
}
