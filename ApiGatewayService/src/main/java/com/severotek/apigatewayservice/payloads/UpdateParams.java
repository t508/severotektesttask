package com.severotek.apigatewayservice.payloads;

public class UpdateParams {
    public String username;

    public String value;

    public UpdateParams (String username, String value){
        this.username = username;
        this.value = value;
    }
}
