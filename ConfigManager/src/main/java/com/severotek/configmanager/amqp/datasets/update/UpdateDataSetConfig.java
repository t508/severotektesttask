package com.severotek.configmanager.amqp.datasets.update;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UpdateDataSetConfig {
    @Bean
    public Queue updateDataSetQueue() {
        return new Queue("updateDataSetQueue");
    }

    @Autowired
    public AmqpAdmin amqpAdmin;

    @Autowired
    public DirectExchange getAllDataSetsExchange;

    @Bean
    public Binding UpdateDataSetBinding() {
        return BindingBuilder.bind(updateDataSetQueue())
                .to(getAllDataSetsExchange)
                .with("update");
    }
}
