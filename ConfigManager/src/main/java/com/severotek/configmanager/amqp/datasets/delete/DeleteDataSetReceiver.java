package com.severotek.configmanager.amqp.datasets.delete;

import com.severotek.configmanager.dao.DataSetDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeleteDataSetReceiver {

    private static final Logger logger = LoggerFactory.getLogger(DeleteDataSetReceiver.class);

    @Autowired
    MessageConverter jsonMessageConverter;

    @RabbitListener(queues = "deleteDataSetQueue")
    public Object getDataSource(Message in) {
        try {
            String name = jsonMessageConverter.fromMessage(in).toString();

            DataSetDAO db = new DataSetDAO();

            var result = db.findByName(name);

            db.delete(result);

            return "DataSet deleted successfully";
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
