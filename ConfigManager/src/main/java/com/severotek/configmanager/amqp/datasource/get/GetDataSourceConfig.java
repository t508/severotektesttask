package com.severotek.configmanager.amqp.datasource.get;


import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GetDataSourceConfig {
    @Bean
    public Queue getDataSourceQueue() {
        return new Queue("getDataSourceQueue");
    }

    @Autowired
    public AmqpAdmin amqpAdmin;

    @Autowired
    public DirectExchange ConnectionToDBExchange;

    @Bean
    public Binding GetDataSourceBinding() {
        return BindingBuilder.bind(getDataSourceQueue())
                .to(ConnectionToDBExchange)
                .with("get");
    }
}
