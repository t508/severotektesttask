package com.severotek.configmanager.amqp.datasets.create;

import com.severotek.configmanager.dao.DataSetDAO;
import com.severotek.configmanager.models.DataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateDataSetsReceiver {
    private static final Logger logger = LoggerFactory.getLogger(CreateDataSetsReceiver.class);

    @Autowired
    MessageConverter jsonMessageConverter;

    @RabbitListener(queues = "createDataSetQueue")
    public Object registerUser(Message in) {
        try {
            DataSet set = (DataSet)jsonMessageConverter.fromMessage(in);

            DataSetDAO db = new DataSetDAO();
            db.save(set);

            return "DataSet successfully created";
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: "  + ex.getMessage();
        }
    }
}
