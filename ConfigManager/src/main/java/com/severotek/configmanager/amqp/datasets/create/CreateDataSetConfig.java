package com.severotek.configmanager.amqp.datasets.create;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

public class CreateDataSetConfig {
    @Bean
    public Queue createDataSetQueue() {
        return new Queue("createDataSetQueue");
    }

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Autowired
    public DirectExchange getAllDataSetsExchange;

    @Bean
    public Binding createUserBinding() {
        return BindingBuilder.bind(createDataSetQueue())
                .to(getAllDataSetsExchange)
                .with("create");
    }
}
