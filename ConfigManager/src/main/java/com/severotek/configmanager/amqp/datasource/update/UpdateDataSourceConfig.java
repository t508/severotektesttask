package com.severotek.configmanager.amqp.datasource.update;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UpdateDataSourceConfig {

    @Bean
    public Queue updateDataSourceQueue() {
        return new Queue("updateDataSourceQueue");
    }

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Autowired
    public DirectExchange ConnectionToDBExchange;

    @Bean
    public Binding updateDataSourceBinding() {
        return BindingBuilder.bind(updateDataSourceQueue())
                .to(ConnectionToDBExchange)
                .with("update");
    }
}
