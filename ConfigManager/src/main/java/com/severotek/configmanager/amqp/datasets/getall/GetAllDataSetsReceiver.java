package com.severotek.configmanager.amqp.datasets.getall;

import com.severotek.configmanager.dao.DataSetDAO;
import com.severotek.configmanager.models.DataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetAllDataSetsReceiver {

    private static final Logger logger = LoggerFactory.getLogger(GetAllDataSetsReceiver.class);

    @Autowired
    MessageConverter jsonMessageConverter;


    @RabbitListener(queues = "getAllDataSetsQueue")
    public Object getAllDataSets(Message in) {
        try {
            String role = jsonMessageConverter.fromMessage(in).toString();

            DataSetDAO db = new DataSetDAO();
            List<DataSet> result = new ArrayList<>();

            switch (role){
                case "ROLE_ADMIN":
                case "ROLE_OPERATOR":
                    result = db.findAll();
                    break;
                case "ROLE_VIEWER":
                    result = db.findAllForViewer();
                    break;
                case "ROLE_GUEST":
                    result = db.findAllForGuest();
                    break;
            }

            return result;
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
