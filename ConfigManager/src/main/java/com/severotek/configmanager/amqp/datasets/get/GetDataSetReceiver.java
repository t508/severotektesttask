package com.severotek.configmanager.amqp.datasets.get;

import com.severotek.configmanager.dao.DataSetDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetDataSetReceiver {
    private static final Logger logger = LoggerFactory.getLogger(GetDataSetReceiver.class);

    @Autowired
    MessageConverter jsonMessageConverter;


    @RabbitListener(queues = "getDataSetQueue")
    public Object getDataSource(Message in) {
        try {
            String name = jsonMessageConverter.fromMessage(in).toString();

            DataSetDAO db = new DataSetDAO();

            var result = db.findByName(name);

            return result;
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
