package com.severotek.configmanager.amqp.datasource.create;

import com.severotek.configmanager.dao.ConnectionToDBDAO;
import com.severotek.configmanager.models.ConnectionToDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CreateDataSourceReceiver {
    private static final Logger logger = LoggerFactory.getLogger(CreateDataSourceReceiver.class);

    @Autowired
    MessageConverter jsonMessageConverter;


    @RabbitListener(queues = "createDBConQueue")
    public Object createCon(Message in) {
        try {
            ConnectionToDB NewCon = (ConnectionToDB)jsonMessageConverter.fromMessage(in);

            ConnectionToDBDAO db = new ConnectionToDBDAO();

            db.save(NewCon);

            return "Connection successfully created";
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
