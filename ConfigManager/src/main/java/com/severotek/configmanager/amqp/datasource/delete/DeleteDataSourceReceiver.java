package com.severotek.configmanager.amqp.datasource.delete;

import com.severotek.configmanager.dao.ConnectionToDBDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeleteDataSourceReceiver {

    private static final Logger logger = LoggerFactory.getLogger(DeleteDataSourceReceiver.class);

    @Autowired
    MessageConverter jsonMessageConverter;


    @RabbitListener(queues = "deleteDataSourceQueue")
    public Object getDataSource(Message in) {
        try {
            String name = jsonMessageConverter.fromMessage(in).toString();

            ConnectionToDBDAO db = new ConnectionToDBDAO();

            var result = db.findByName(name);

            db.delete(result);

            return "DataSource deleted successfully";
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
