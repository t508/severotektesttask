package com.severotek.configmanager.amqp.datasets.getall;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;


public class GetAllDataSetsConfig {

    @Bean
    public Queue getAllDataSetsQueue() {
        return new Queue("getAllDataSetsQueue");
    }

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Bean
    public DirectExchange getAllDataSetsExchange() {
        return new DirectExchange("dataSetsController");
    }

    @Bean
    public Binding createUserBinding() {
        return BindingBuilder.bind(getAllDataSetsQueue())
                .to(getAllDataSetsExchange())
                .with("getAll");
    }
}
