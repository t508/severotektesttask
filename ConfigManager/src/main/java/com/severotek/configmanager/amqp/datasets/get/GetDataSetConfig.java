package com.severotek.configmanager.amqp.datasets.get;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

public class GetDataSetConfig {
    @Bean
    public Queue getDataSetQueue() {
        return new Queue("getDataSetQueue");
    }

    @Autowired
    public AmqpAdmin amqpAdmin;

    @Autowired
    public DirectExchange getAllDataSetsExchange;

    @Bean
    public Binding GetDataSourceBinding() {
        return BindingBuilder.bind(getDataSetQueue())
                .to(getAllDataSetsExchange)
                .with("get");
    }
}
