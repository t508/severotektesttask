package com.severotek.configmanager.amqp.datasource.getall;

import com.severotek.configmanager.dao.ConnectionToDBDAO;
import com.severotek.configmanager.models.ConnectionToDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetAllDataSourceReceiver {

    private static final Logger logger = LoggerFactory.getLogger(GetAllDataSourceReceiver.class);

    @Autowired
    MessageConverter jsonMessageConverter;


    @RabbitListener(queues = "getAllQueue")
    public Object getAllConnections(Message in) {
        try {
            String role = jsonMessageConverter.fromMessage(in).toString();

            ConnectionToDBDAO db = new ConnectionToDBDAO();
            List<ConnectionToDB> result = new ArrayList<>();

            switch (role){
                case "ROLE_ADMIN":
                case "ROLE_OPERATOR":
                    result = db.findAll();
                    break;
                case "ROLE_VIEWER":
                    result = db.findAllForViewer();
                    break;
                case "ROLE_GUEST":
                    result = db.findAllForGuest();
                    break;
            }

            return result;
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
