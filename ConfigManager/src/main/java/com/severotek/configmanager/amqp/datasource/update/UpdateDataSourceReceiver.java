package com.severotek.configmanager.amqp.datasource.update;

import com.severotek.configmanager.dao.ConnectionToDBDAO;
import com.severotek.configmanager.models.ConnectionToDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdateDataSourceReceiver {

    private static final Logger logger = LoggerFactory.getLogger(UpdateDataSourceReceiver.class);

    @Autowired
    MessageConverter jsonMessageConverter;

    @RabbitListener(queues = "updateDataSourceQueue")
    public Object getDataSource(Message in) {
        try {
            ConnectionToDB con = (ConnectionToDB)jsonMessageConverter.fromMessage(in);

            ConnectionToDBDAO db = new ConnectionToDBDAO();

            var result = db.findByName(con.getName());

            con.setId(result.getId());

            db.update(con);

            return "DataSource updated successfully";
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
