package com.severotek.configmanager.amqp.datasource.delete;


import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DeleteDataSourceConfig {

    @Bean
    public Queue deleteDataSourceQueue() {
        return new Queue("deleteDataSourceQueue");
    }

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Autowired
    public DirectExchange ConnectionToDBExchange;

    @Bean
    public Binding deleteDataSourceBinding() {
        return BindingBuilder.bind(deleteDataSourceQueue())
                .to(ConnectionToDBExchange)
                .with("delete");
    }
}
