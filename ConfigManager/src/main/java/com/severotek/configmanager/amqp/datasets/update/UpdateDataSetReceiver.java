package com.severotek.configmanager.amqp.datasets.update;

import com.severotek.configmanager.dao.DataSetDAO;
import com.severotek.configmanager.models.DataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdateDataSetReceiver {

    private static final Logger logger = LoggerFactory.getLogger(UpdateDataSetReceiver.class);

    @Autowired
    MessageConverter jsonMessageConverter;


    @RabbitListener(queues = "updateDataSetQueue")
    public Object getDataSource(Message in) {
        try {
            DataSet con = (DataSet)jsonMessageConverter.fromMessage(in);

            DataSetDAO db = new DataSetDAO();

            var result = db.findByName(con.getName());

            con.setId(result.getId());

            db.update(con);

            return "DataSet updated successfully";
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
