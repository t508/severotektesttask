package com.severotek.configmanager.amqp.datasets.delete;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DeleteDataSetConfig {
    @Bean
    public Queue deleteDataSetQueue() {
        return new Queue("deleteDataSetQueue");
    }

    @Autowired
    public AmqpAdmin amqpAdmin;

    @Autowired
    public DirectExchange getAllDataSetsExchange;

    @Bean
    public Binding DeleteDataSetBinding() {
        return BindingBuilder.bind(deleteDataSetQueue())
                .to(getAllDataSetsExchange)
                .with("delete");
    }
}
