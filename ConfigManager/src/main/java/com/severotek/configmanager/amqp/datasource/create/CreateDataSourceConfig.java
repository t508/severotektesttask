package com.severotek.configmanager.amqp.datasource.create;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

public class CreateDataSourceConfig {

    @Bean
    public Queue createDBConnection() {
        return new Queue("createDBConQueue");
    }

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Autowired
    public DirectExchange ConnectionToDBExchange;

    @Bean
    public Binding createUserBinding() {
        return BindingBuilder.bind(createDBConnection())
                .to(ConnectionToDBExchange)
                .with("create");
    }
}
