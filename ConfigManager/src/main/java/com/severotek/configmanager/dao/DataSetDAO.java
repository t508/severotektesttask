package com.severotek.configmanager.dao;

import com.severotek.configmanager.HibernateSessionFactoryUtil;
import com.severotek.configmanager.models.DataSet;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class DataSetDAO {
    public DataSet findByName(String name) {
        return (DataSet)HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From DataSet where name = :name")
                .setParameter("name", name)
                .list()
                .get(0);
    }

    public void save(DataSet set) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(set);
        tx1.commit();
        session.close();
    }

    public void update(DataSet set) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(set);
        tx1.commit();
        session.close();
    }

    public void delete(DataSet set) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(set);
        tx1.commit();
        session.close();
    }

    public List<DataSet> findAll() {
        List<DataSet> connections = (List<DataSet>)HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From DataSet")
                .list();
        return connections;
    }

    public List<DataSet> findAllForViewer() {
        List<DataSet> connections = (List<DataSet>)HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From DataSet where isViewerAllowed = true")
                .list();
        return connections;
    }

    public List<DataSet> findAllForGuest() {
        List<DataSet> connections = (List<DataSet>)HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From DataSet where isGuestAllowed = true")
                .list();
        return connections;
    }
}
