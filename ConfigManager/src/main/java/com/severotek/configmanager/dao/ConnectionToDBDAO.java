package com.severotek.configmanager.dao;

import com.severotek.configmanager.HibernateSessionFactoryUtil;
import com.severotek.configmanager.models.ConnectionToDB;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ConnectionToDBDAO {

    public ConnectionToDB findByName(String NameForSearch) {
        return (ConnectionToDB)HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From ConnectionToDB where name = :NameForSearch")
                .setParameter("NameForSearch", NameForSearch)
                .list()
                .get(0);
    }

    public void save(ConnectionToDB Connection) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(Connection);
        tx1.commit();
        session.close();
    }

    public void update(ConnectionToDB Connection) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(Connection);
        tx1.commit();
        session.close();
    }

    public void delete(ConnectionToDB Connection) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(Connection);
        tx1.commit();
        session.close();
    }

    public List<ConnectionToDB> findAll() {
        List<ConnectionToDB> connections = (List<ConnectionToDB>)HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From ConnectionToDB")
                .list();
        return connections;
    }

    public List<ConnectionToDB> findAllForViewer() {
        List<ConnectionToDB> connections = (List<ConnectionToDB>)HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From ConnectionToDB where isViewerAllowed = true")
                .list();
        return connections;
    }

    public List<ConnectionToDB> findAllForGuest() {
        List<ConnectionToDB> connections = (List<ConnectionToDB>)HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From ConnectionToDB where isGuestAllowed = true")
                .list();
        return connections;
    }
}