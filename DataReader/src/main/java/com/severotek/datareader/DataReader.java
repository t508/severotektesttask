package com.severotek.datareader;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataReader {
    public String url;

    public String user;

    public String password;

    public String command;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public DataReader(){}

    public DataReader(String url, String user, String password, String command){
        this.url = url;
        this.user = user;
        this.password = password;
        this.command = command;
    }

    public Object ReadDataFromDB()  throws SQLException{
        try {
            Connection con = DriverManager.getConnection(url, user, password);
            PreparedStatement statement = con.prepareStatement(command);

            boolean hasResult = statement.execute();
            if (hasResult) {
                ResultSet resultSet = statement.getResultSet();
                ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
                final int columnCount = resultSetMetaData.getColumnCount();

                List<Object[]> result = new ArrayList<>();

                while (resultSet.next()) {
                    Object[] values = new Object[columnCount];
                    for (int i = 1; i <= columnCount; i++) {
                        values[i - 1] = resultSet.getObject(i);
                    }
                    result.add(values);
                }
                return result;
            }
            else
                return "No rows";
        }
        catch (Exception ex){
            return "Error: " + ex.getMessage();
        }
    }
}
