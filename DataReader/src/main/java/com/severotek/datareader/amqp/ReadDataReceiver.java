package com.severotek.datareader.amqp;

import com.severotek.datareader.DataReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ReadDataReceiver {

    private static final Logger logger = LoggerFactory.getLogger(ReadDataReceiver.class);

    @Autowired
    MessageConverter jsonMessageConverter;


    @RabbitListener(queues = "readDataQueue")
    public Object registerUser(Message in) {
        try {
            DataReader dr = (DataReader)jsonMessageConverter.fromMessage(in);
            var result = dr.ReadDataFromDB();
            return result;
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
