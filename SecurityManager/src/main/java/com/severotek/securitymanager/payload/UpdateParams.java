package com.severotek.securitymanager.payload;

public class UpdateParams {
    public String username;

    public String value;

    public UpdateParams(){}

    public UpdateParams(String username, String value){
        this.username = username;
        this.value = value;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
