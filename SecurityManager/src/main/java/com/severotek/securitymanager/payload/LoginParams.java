package com.severotek.securitymanager.payload;

public class LoginParams {
    public String username;

    public String password;

    public LoginParams(){

    }

    public LoginParams(String _username, String _password){
        this.username = _username;
        this.password = _password;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }
}
