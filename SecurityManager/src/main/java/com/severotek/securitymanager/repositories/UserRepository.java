package com.severotek.securitymanager.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.severotek.securitymanager.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    @Transactional
    @Modifying
    @Query("update User u set u.role = ?2 where u.username = ?1")
    void setUserRoleByUserName(String username, String role);

    @Transactional
    @Modifying
    @Query("update User u set u.username = ?2 where u.username = ?1")
    void setUserNameByUserName(String oldUsername, String newUsername);
}