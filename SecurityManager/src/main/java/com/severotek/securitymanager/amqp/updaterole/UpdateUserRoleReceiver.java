package com.severotek.securitymanager.amqp.updaterole;

import com.severotek.securitymanager.payload.UpdateParams;
import com.severotek.securitymanager.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UpdateUserRoleReceiver {

    private static final Logger logger = LoggerFactory.getLogger(UpdateUserRoleReceiver.class);

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    MessageConverter jsonMessageConverter;

    @Autowired
    UserRepository userRepository;

    @RabbitListener(queues = "updateUserRoleQueue")
    public String registerUser(Message in) {
        try {
            UpdateParams params = (UpdateParams) jsonMessageConverter.fromMessage(in);


            userRepository.setUserRoleByUserName(params.getUsername(), params.getValue());

            return "User role updated successfully!";
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }

}
