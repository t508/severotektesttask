package com.severotek.securitymanager.amqp.updateusername;

import com.severotek.securitymanager.amqp.updaterole.UpdateUserRoleReceiver;
import com.severotek.securitymanager.payload.UpdateParams;
import com.severotek.securitymanager.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UpdateUsernameReceiver {

    private static final Logger logger = LoggerFactory.getLogger(UpdateUsernameReceiver.class);

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    MessageConverter jsonMessageConverter;

    @Autowired
    UserRepository userRepository;

    @RabbitListener(queues = "updateUsernameQueue")
    public String registerUser(Message in) {
        try {
            UpdateParams params = (UpdateParams) jsonMessageConverter.fromMessage(in);


            userRepository.setUserNameByUserName(params.getUsername(), params.getValue());

            return "Username updated successfully!";
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
