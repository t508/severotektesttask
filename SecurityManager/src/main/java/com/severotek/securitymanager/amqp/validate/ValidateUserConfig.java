package com.severotek.securitymanager.amqp.validate;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ValidateUserConfig {
    @Bean
    public Queue validateUserQueue() {
        return new Queue("validateUserQueue");
    }

    @Autowired
    public DirectExchange loginExchange;

    @Bean
    public Binding validateUserBinding() {
        return BindingBuilder.bind(validateUserQueue())
                .to(loginExchange)
                .with("validate");
    }
}
