package com.severotek.securitymanager.amqp.create;

import com.severotek.securitymanager.models.User;
import com.severotek.securitymanager.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class CreateUserReceiver {

    private static final Logger logger = LoggerFactory.getLogger(CreateUserReceiver.class);

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    MessageConverter jsonMessageConverter;

    @Autowired
    UserRepository userRepository;

    @RabbitListener(queues = "createUserQueue")
    public String registerUser(Message in) {
        try {
            User params = (User) jsonMessageConverter.fromMessage(in);
            String username = params.getUsername();
            String password = params.getPassword();
            String role = params.getRole();

            if (userRepository.existsByUsername(username)) {
                return "Error: Username is already taken!";
            }

            User user = new User(username,
                    encoder.encode(password), role);

            userRepository.save(user);

            return "User registered successfully!";
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
