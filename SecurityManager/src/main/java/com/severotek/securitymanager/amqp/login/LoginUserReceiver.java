package com.severotek.securitymanager.amqp.login;

import com.severotek.securitymanager.payload.LoginParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import com.severotek.securitymanager.jwt.JwtUtils;

@Component
public class LoginUserReceiver {
    private static final Logger logger = LoggerFactory.getLogger(LoginUserReceiver.class);

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    MessageConverter jsonMessageConverter;



    @RabbitListener(queues = "GatewaySecQueue")
    public String loginUser(Message in) {
        try {
            LoginParams params = (LoginParams) jsonMessageConverter.fromMessage(in);
            String username = params.getUsername();
            String password = params.getPassword();
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username, password));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            return jwt;
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
