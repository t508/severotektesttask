package com.severotek.securitymanager.amqp.updaterole;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UpdateUserRoleConfig {
    @Bean
    public Queue updateUserRoleQueue() {
        return new Queue("updateUserRoleQueue");
    }

    @Autowired
    public DirectExchange loginExchange;

    @Bean
    public Binding updateUserRoleBinding() {
        return BindingBuilder.bind(updateUserRoleQueue())
                .to(loginExchange)
                .with("updateRole");
    }
}
