package com.severotek.securitymanager.amqp.delete;

import com.severotek.securitymanager.models.User;
import com.severotek.securitymanager.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DeleteUserReceiver {

    private static final Logger logger = LoggerFactory.getLogger(DeleteUserReceiver.class);

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    MessageConverter jsonMessageConverter;

    @Autowired
    UserRepository userRepository;

    @RabbitListener(queues = "deleteUserQueue")
    public String registerUser(Message in) {
        try {
            String username = jsonMessageConverter.fromMessage(in).toString();

            if (!userRepository.existsByUsername(username)) {
                return "Error: no such user!";
            }

            Optional<User> _user = userRepository.findByUsername(username);

            userRepository.delete(_user.get());

            return "User deleted successfully!";
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
