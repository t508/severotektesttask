package com.severotek.securitymanager.amqp.create;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CreateUserConfig {

    @Bean
    public Queue createUserQueue() {
        return new Queue("createUserQueue");
    }

    @Autowired
    public DirectExchange loginExchange;

    @Bean
    public Binding createUserBinding() {
        return BindingBuilder.bind(createUserQueue())
                .to(loginExchange)
                .with("create");
    }
}
