package com.severotek.securitymanager.amqp.login;

import com.severotek.securitymanager.models.User;
import com.severotek.securitymanager.payload.LoginParams;
import com.severotek.securitymanager.payload.UpdateParams;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.DefaultClassMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class LoginAndMainConfig {
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory =
                new CachingConnectionFactory("localhost");
        return connectionFactory;
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Queue loginQueue() {
        return new Queue("GatewaySecQueue");
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        Jackson2JsonMessageConverter jsonConverter = new Jackson2JsonMessageConverter();
        jsonConverter.setClassMapper(classMapper());
        return jsonConverter;
    }

    @Bean
    public DefaultClassMapper classMapper()
    {
        DefaultClassMapper classMapper = new DefaultClassMapper();
        Map<String, Class<?>> idClassMapping = new HashMap<>();
        idClassMapping.put("com.severotek.apigatewayservice.payloads.LoginParams", LoginParams.class);
        idClassMapping.put("com.severotek.apigatewayservice.models.User", User.class);
        idClassMapping.put("com.severotek.apigatewayservice.payloads.UpdateParams", UpdateParams.class);
        classMapper.setIdClassMapping(idClassMapping);
        return classMapper;
    }

    @Bean
    public DirectExchange loginExchange() {
        return new DirectExchange("userController");
    }

    @Bean
    public Binding binding() {
        return BindingBuilder.bind(loginQueue())
                .to(loginExchange())
                .with("login");
    }
}
