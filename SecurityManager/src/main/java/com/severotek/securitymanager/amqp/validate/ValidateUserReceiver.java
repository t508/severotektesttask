package com.severotek.securitymanager.amqp.validate;

import com.severotek.securitymanager.amqp.create.CreateUserReceiver;
import com.severotek.securitymanager.jwt.JwtUtils;
import com.severotek.securitymanager.models.User;
import com.severotek.securitymanager.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class ValidateUserReceiver {

    private static final Logger logger = LoggerFactory.getLogger(CreateUserReceiver.class);

    @Autowired
    MessageConverter jsonMessageConverter;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtUtils jwtUtils;

    @RabbitListener(queues = "validateUserQueue")
    public String validateUser(Message in) {
        try {
            String jwt = (String) jsonMessageConverter.fromMessage(in);
            if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
                String username = jwtUtils.getUserNameFromJwtToken(jwt);

                User _user = userRepository.findByUsername(username).get();
                return _user.getRole();
            }
            else
                return "Error: Wrong token";
        }
        catch(Exception ex){
            logger.info(ex.getMessage());
            return "Error: " + ex.getMessage();
        }
    }
}
