package com.severotek.securitymanager.amqp.updateusername;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UpdateUsernameConfig {

    @Bean
    public Queue updateUsernameQueue() {
        return new Queue("updateUsernameQueue");
    }

    @Autowired
    public DirectExchange loginExchange;

    @Bean
    public Binding updateUsernameBinding() {
        return BindingBuilder.bind(updateUsernameQueue())
                .to(loginExchange)
                .with("updateUsername");
    }
}
