package com.severotek.securitymanager.amqp.get;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GetUserConfig {
    @Bean
    public Queue getUserQueue() {
        return new Queue("getUserQueue");
    }

    @Autowired
    public DirectExchange loginExchange;

    @Bean
    public Binding getUserBinding() {
        return BindingBuilder.bind(getUserQueue())
                .to(loginExchange)
                .with("get");
    }
}
