package com.severotek.securitymanager.amqp.delete;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DeleteUserConfig {
    @Bean
    public Queue deleteUserQueue() {
        return new Queue("deleteUserQueue");
    }

    @Autowired
    public DirectExchange loginExchange;

    @Bean
    public Binding deleteUserBinding() {
        return BindingBuilder.bind(deleteUserQueue())
                .to(loginExchange)
                .with("delete");
    }
}
